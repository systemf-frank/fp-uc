#define F_CPU 16000000UL

#include <avr/io.h>
#include <util/delay.h>
#include <stdbool.h>

#define BUTTON PB0 /* pin 8 */
#define LED PB5 /* pin 13 */

int
main (void)
{
	DDRB |= (1 << LED);
	PORTB |= (1 << BUTTON); /* Turn on pull-up resistor */

	while(1)
	{
		if ((PINB & (1 << BUTTON))) {
			PORTB |= (1 << LED);
		} else {
			PORTB &= ~(1 << LED);
		}
	}
}
