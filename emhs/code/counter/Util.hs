{-# LANGUAGE MultiParamTypeClasses, FunctionalDependencies #-}

module Util where

{-
 - This file is specific for the Atmel ATmega 328P based boards. This includes
 - most Arduino's.
-}

import Control.Monad.State
import Control.Monad.Writer
import Data.List (groupBy)

import Language.Copilot as CP hiding ((++))
import Copilot.Compile.C99

import Prelude as P hiding (cycle)

import ATmega328P

import Text.Printf

type C = String

-- Data structure used te keep track of what we have to write to our C file.
-- Inputs take a default value.
data IOSpec = IOSpec {
    outpins :: [(Pin,Stream Bool)],
    inpins  :: [(Pin,Bool)],
    outports :: [(Port,Stream Word8)],
    inports :: [(Port,Word8)]
}
type Program a = State IOSpec a

-- Class of types that can be interpreted as output
-- NOTE: this function does have to return a stream as the return value of the
-- State. This will never be used however, and is only there because we have to
-- return one.
class Output ty cty | ty -> cty where
    (.=) :: ty -> Stream cty -> Program (Stream cty)

-- Class of types that can be interpreted as input.
-- input takes a pin/port, default value and returns a stream.
class Input ty cty | ty -> cty where
    input :: ty -> cty -> Program (Stream cty)

-- Like stated above, we return the stream, but we will never use it.
-- We only return one, because there exists no identity stream.
instance Output Pin Bool where
    pin .= stream = state (\spec ->
                            let spec' = spec {outpins = (pin,stream):outpins spec}
                            in (stream, spec'))

instance Output Port Word8 where
    port .= stream = state (\spec ->
                             let spec' = spec {outports = (port,stream):outports spec}
                             in (stream, spec'))

-- The stream we return will be read from a C value using Copilots 'extern'.
instance Input Pin Bool where
    input pin def = state (\spec ->
                            let spec' = spec {inpins = (pin,def):inpins spec}
                            in (extern ("v" ++ show pin) Nothing, spec'))

instance Input Port Word8 where
    input port def = state (\spec ->
                             let spec' = spec {inports = (port,def):inports spec}
                             in (extern ("v" ++ show port) Nothing, spec'))

-- Generate Copilot and main code
gencode :: FilePath -> Program a -> IO ()
gencode outfile p = do
    let (_,pinspec) = runState p idiospec
    let maincode = ccode pinspec (mhz 16)
    let spec = genspec pinspec
    reify spec >>= compile defaultParams
    writeFile outfile maincode

-- Identity IOSpec
idiospec :: IOSpec
idiospec = IOSpec [] [] [] []

ccode :: IOSpec -> Int -> C
ccode iospec clockspeed = unlines [
    "#define F_CPU " P.++ show clockspeed P.++ "UL",
    unlines cincludes,

    -- Write functions setting the values
    unlines $ map pinfunc (map fst (outpins iospec)),
    unlines $ map portfunc (map fst (outports iospec)),

    -- Define variables used for input
    unlines $ map pindef (inpins iospec),
    unlines $ map portdef (inports iospec),

    "void setup() {",
        -- Set DDR registers
        -- We will only handle outputs, as pins and ports are input per default
        unlines $ pinddr (map fst (outpins iospec)),
        unlines $ portddr (map fst (outports iospec)),
    "}",
    "void main() {",
        "setup();",
        "for (;;) {",
            -- Update variables
            unlines $ pinupdate (map fst (inpins iospec)),
            unlines $ portupdate (map fst (inports iospec)),

            "step();", -- Copilot step() function
        "}",
    "}"
    ] where
        pinfunc p = printf "void set_%s (%s val) { %s ^= (-val ^ %s) & (1 << %s); }" (show p) "bool" (show $ portof p) (show $ portof p) (show p)
        portfunc p = csetfunc (show p) "uint8_t"

        pindef (pin,val) = cvardef "bool" ("v" ++ show pin) (cbool val)
        portdef (port,val) = cvardef "uint8_t" ("v" ++ show port) (show val)

        pinddr pins = map (\(port, val) -> cassign (show $ ddr port) (show $ val)) vals where
            pins' = map (\p -> (portof p, bitvalue p)) pins -- Combine value with port
            grouped = groupBy (\x y -> fst x P.== fst y) pins'
            vals :: [(Port, Word8)]
            vals = map plus grouped
            plus xs = foldr (\x y -> (fst x, snd x + snd y)) (PORTB,0) xs

        portddr ports = map (\(port,val) -> cassign (show $ ddr port) (show val)) vals where
            vals :: [(Port, Word8)]
            vals = map (\x -> (x, maxvalue x)) ports

        pinupdate :: [Pin] -> [C]
        pinupdate ps = map (\x -> cassign ("v" ++ show x) (show (inputof $ portof x) ++ " & (1 << " ++ show x ++")")) ps

        portupdate :: [Port] -> [C]
        portupdate ps = map (\x -> cassign ("v" ++ show x) (show (inputof x))) ps

cincludes :: [C]
cincludes = [
    "#include \"copilot-c99-codegen/copilot.h\"",
    "#include <avr/io.h>",
    "#include <stdbool.h>",
    "#include <util/delay.h>",
    "#include <stdio.h>"
    ]

-- Turns a speed in Mhz into hz
mhz :: Int -> Int
mhz clk = 1000000 * clk

cvardef :: String -> String -> String -> C
cvardef ty var val = printf "%s %s = %s;" ty var val

csetfunc :: String -> String -> C
csetfunc var ty = printf "void set_%s (%s val) { %s = val; }" var ty var

cassign :: String -> String -> C
cassign var val = printf "%s = %s;" var val

-- Convert a Bool to an Int
cbool :: Bool -> C
cbool True = "true"
cbool False = "false"

-- Generate Copilot spec form IOSpec
genspec :: IOSpec -> Spec
genspec iospec = appendspec spec' spec'' where
    pintrg x = map (\(pin,stream) -> trigger ("set_" ++ show pin) true [arg stream]) x
    spec' = foldr appendspec idspec (pintrg (outpins iospec))
    spec'' = foldr appendspec idspec (pintrg (outports iospec))

-- Identity Copilot spec
idspec :: Spec
idspec = writer ((), [])

-- Append two Copilot spec's
appendspec :: Spec -> Spec -> Spec
appendspec s1 s2 = writer ((), s1' P.++ s2') where
    s1' = snd $ runWriter s1
    s2' = snd $ runWriter s2
