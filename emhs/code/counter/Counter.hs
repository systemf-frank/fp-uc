module Main where

import Language.Copilot

import qualified Prelude as P

import Util hiding (spec)
import ATmega328P

-- Integer values corresponding bits of a port
a, b, c, d, e, f, g, dp :: Word8
g = 1
f = 2
a = 4
b = 8
d = 16
e = 32
c = 64
dp = 128

-- Translate a character to bars on a single display
digit8 :: Char -> Word8
digit8 char = case char of
    '0' -> foldr (+) 0 [a,b,c,d,e,f]
    '1' -> foldr (+) 0 [b,c]
    '2' -> foldr (+) 0 [a,b,g,e,d]
    '3' -> foldr (+) 0 [a,b,c,d,g]
    '4' -> foldr (+) 0 [f,g,b,c]
    '5' -> foldr (+) 0 [a,f,g,c,d]
    '6' -> foldr (+) 0 [a,f,g,c,d]
    '7' -> foldr (+) 0 [a,b,c]
    '8' -> foldr (+) 0 [a,b,c,d,e,f,g]
    '9' -> foldr (+) 0 [a,b,c,d,f,g]
    'a' -> foldr (+) 0 [a,b,c,e,f,g]
    'b' -> foldr (+) 0 [f,g,c,d,e]
    'c' -> foldr (+) 0 [a,f,e,d]
    'd' -> foldr (+) 0 [b,c,d,e,g]
    'e' -> foldr (+) 0 [a,f,g,e,d]
    'f' -> foldr (+) 0 [a,f,g,e]
    '.' -> dp

-- Cathodes denote which display is lit
cathodes :: Stream Word8
cathodes = complement $ cycle [1,2,4,8]

-- Create a counter, repeating every element 40 times
-- This is a cheap but ineffecient way to make the counter slower
counter :: [Word16]
counter = counter' 0 where
    counter' :: Word16 -> [Word16]
    counter' n | n P.> 10 = []
               | otherwise = replicate 40 n P.++ (counter' (n+1))

-- Really simple way to translate 4 characters to 4 Word8's
-- If less than 4 characters are given, the display is right-aligned
display :: [Char] -> [Word8]
display (w:x:y:z:[]) = [digit8 w, digit8 x, digit8 y, digit8 z]
display (x:y:z:[])   = [0, digit8 x, digit8 y, digit8 z]
display (y:z:[])     = [0, 0, digit8 y, digit8 z]
display (z:[])       = [0, 0, 0, digit8 z]
display _            = undefined

-- Anodes denote the characters that are displayed
anodes :: Stream Word8
anodes = cycle $ concatMap (\x -> display (show x)) counter

-- Main program
-- Note that when PORTC is low/False, the display is turned on!
-- This is due to the wiring
program = do
    button <- input PB0 False

    PORTC .= mux button cathodes (cycle [255])
    PORTD .= anodes

main = gencode "main.c" program
